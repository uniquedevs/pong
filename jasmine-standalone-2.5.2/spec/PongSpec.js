/**
 * Created by max on 4/14/17.
 */
describe('Ball', function(){
    let CLASSES = APP.module('Classes'),
        ball;
    beforeEach(function() {
        ball = new CLASSES.Ball();
    });

    describe("when new ball instantiates", function() {

        it("should return initial velocity", function () {
            expect(ball.getVelocity()).toEqual([0, 0]);
        });
    });

    describe("when balls properties updates", function() {
        it("should return new velocity", function () {
            ball.setVelocity([1,1]);
            expect(ball.getVelocity()).toEqual([1,1]);
        });
        it("should return new position when move", function () {
            ball.setVelocity([1,-1]).setPosition([100,100]);
            ball.move();
            expect(ball.getPosition()).toEqual([101,99]);
        });
    });

    describe("when several instances are instantiated", function() {

        let new_ball;

        beforeEach(function() {
            new_ball = new CLASSES.Ball();
        });

        it("when balls velocity updates it should not effects other balls velocity", function () {
            ball.setVelocity([1,1]);
            expect(new_ball.getVelocity()).toEqual([0, 0]);
            expect(ball.getVelocity()).toEqual([1, 1]);
        });
    });

    describe("when ball reflect from borders", function(){
        let normal;
        it("should reflect from top border", function(){
            normal = [0,1];
            ball.setVelocity([1,-3]);
            ball.reflect(normal);
            expect(ball.getVelocity()).toEqual([1,3]);
            ball.setVelocity([-1,-3]);
            ball.reflect(normal);
            expect(ball.getVelocity()).toEqual([-1,3]);
        });
        it("should reflect from left border", function(){
            normal = [-1,0];
            ball.setVelocity([1,-3]);
            ball.reflect(normal);
            expect(ball.getVelocity()).toEqual([-1,-3]);
            ball.setVelocity([1,3]);
            ball.reflect(normal);
            expect(ball.getVelocity()).toEqual([-1,3]);
        });
        it("should reflect from bottom border", function(){
            normal = [0,-1];
            ball.setVelocity([1,3]);
            ball.reflect(normal);
            expect(ball.getVelocity()).toEqual([1,-3]);
            ball.setVelocity([-1,3]);
            ball.reflect(normal);
            expect(ball.getVelocity()).toEqual([-1,-3]);
        });
        it("should reflect from left border", function(){
            normal = [1,0];
            ball.setVelocity([-1,3]);
            ball.reflect(normal);
            expect(ball.getVelocity()).toEqual([1,3]);
            ball.setVelocity([-1,-3]);
            ball.reflect(normal);
            expect(ball.getVelocity()).toEqual([1,-3]);
        });
    })
});

describe('Game', function(){
    let CLASSES = APP.module('Classes'),
        game, ball, paddle_left, paddle_right
    ;
    beforeEach(function() {
        game = new CLASSES.Game();
    });
    describe("when new game start", function() {
        beforeEach(function() {
            game.start(600, 400);
        });
        it("should return correct left paddle position", function () {
            expect(game.paddle_left.getPosition()).toEqual([0, 200]);
        });
        it("should return correct right paddle position", function () {
            expect(game.paddle_right.getPosition()).toEqual([594, 200]);
        });
        it("should return correct ball velocity", function () {
            _.forEach(_.range(100), function(){
                game.start(600, 400);
                expect(game.ball.getVelocity()[0]).toBeLessThanOrEqual(5);
                expect(game.ball.getVelocity()[0]).toBeGreaterThanOrEqual(-5);
                expect(game.ball.getVelocity()[1]).toBeLessThanOrEqual(5);
                expect(game.ball.getVelocity()[1]).toBeGreaterThanOrEqual(-5);
            });
        });
        it("should return correct ball position after first tick", function(){
            game.tick();

            expect(game.ball.getPosition()[0]).not.toEqual(300);
            expect(game.ball.getPosition()[1]).not.toEqual(200);
            expect(game.ball.getPosition()[0]).toBeLessThanOrEqual(305);
            expect(game.ball.getPosition()[0]).toBeGreaterThanOrEqual(295);
            expect(game.ball.getPosition()[1]).toBeLessThanOrEqual(205);
            expect(game.ball.getPosition()[1]).toBeGreaterThanOrEqual(195);

        })
    });
});