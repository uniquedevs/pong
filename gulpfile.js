'use strict';

const   gulp = require('gulp'),
        jasmine = require('gulp-jasmine')
;

gulp.task('test', () =>
    gulp.src(['app/js/**/*.js','spec/module.js'])
    // gulp-jasmine works on filepaths so you can't have any plugins before it
        .pipe(jasmine())
);