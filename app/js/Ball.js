'use strict'

;(function(APP){
    let CLASSES = APP.module('Classes'),
        dot = function( x, y ){
            return x[0]*y[0] + x[1]*y[1];
        }
    ;
    CLASSES.Ball = function(radius){
        // private properties
        let _velocity = [0,0],
            _boost = 1,
            _position = [0,0],
            _radius = radius
        ;
        // getters
        this.getVelocity = function(){
            return _velocity;
        };
        this.getPosition = function(){
            return _position;
        };
        this.getRadius = function(){
            return _radius;
        };
        // setters
        this.setVelocity = function(velocity){
            _velocity = velocity;
            return this;
        };
        this.setPosition = function(position){
            _position = position;
            return this;
        };
        this.setRadius = function(radius){
            _radius = radius;
            return this;
        };
        this.move = function(){
            _position[0] += _velocity[0];
            _position[1] += _velocity[1];

            return this;
        };
        // this.boost = function( delta ){
        //     _boost +=  delta;
        //     return this;
        // };
        this.reflect = function( normal ){
            // https://habrahabr.ru/post/105882/
            let length = dot(_velocity, normal);
            _velocity[0] = _velocity[0] - 2 * length * normal[0];
            _velocity[1] = _velocity[1] - 2 * length * normal[1];

            return this;
        }
    };
})(window.APP);