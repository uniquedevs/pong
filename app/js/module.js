'use strict';

let APP = window.APP = {};

(function(APP){
	APP.module = function(module_name){
        return APP.module[module_name] ? APP.module[module_name] : APP.module[module_name] = {};
	}
})(window.APP);