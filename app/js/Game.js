'use strict'

;(function(APP){

    let CLASSES = APP.module('Classes'),
        Ball = CLASSES.Ball,
        Paddle = CLASSES.Paddle,
        rand = function(){
            // range from -5 to 5
            let range_x = _.range(-5,-3).concat(_.range(4,6)),
                range_y = _.range(-2,0).concat(_.range(1,2))
            ;
            return [range_x[Math.floor(Math.random() * 3)], range_y[Math.floor(Math.random() * 3)]];
        }
    ;
    CLASSES.Game = function(width, height, ball_radius, paddle_width, paddle_height){
        this.defineCollideNormal = function(){
            let unit_normal,
                ball = this.ball,
                pl = this.paddle_left,
                pr = this.paddle_right,
                ball_position = ball.getPosition(),
                paddle_left_y_position = pl.getPosition()[1],
                paddle_right_y_position = pr.getPosition()[1]
            ;
            //left side collide
            if( ball_position[0] - ball_radius <= paddle_width ) {
                //left paddle collide
                if( Math.abs(paddle_left_y_position - ball_position[1]) <= paddle_height/2 ) {
                    unit_normal = [-1,0];
                    this.ball.setVelocity([this.ball.getVelocity()[0]*1.1, this.ball.getVelocity()[1]*1.1]);
                }
                else {
                    this.goal( 'left' );
                    return false;
                }
            }
            //right side collide
            if( ball_position[0] + ball_radius >= width - paddle_width ){
                //right paddle collide
                if( Math.abs(paddle_right_y_position - ball_position[1]) <= paddle_height/2 ){
                    unit_normal = [1,0];
                    this.ball.setVelocity([this.ball.getVelocity()[0]*1.1, this.ball.getVelocity()[1]*1.1]);
                }
                else {
                    this.goal( 'right' );
                    return false;
                }
            }
            // top side collide
            if( ball_position[1] - ball_radius <= 0 ) {
                unit_normal = [0,1];
            }
            // top side collide
            if( ball_position[1] + ball_radius >= height ) {
                unit_normal = [0,-1];
            }
            return unit_normal;
        };
        this.goal = function(player){
            console.log('goal to: ', player );
            this.restart();
        };
        this.setPaddleVelocity = function( paddle ){
            let
                velocity = paddle.getVelocity(),
                paddle_position = paddle.getPosition()[1],
                paddle_new_position = paddle_position + velocity,
                half_paddle_height = paddle.getHeight()/2
            ;
            if( paddle_new_position > height - half_paddle_height ){
                velocity = paddle_new_position - height + half_paddle_height;
            }
            if( paddle_new_position < half_paddle_height ){
                velocity = paddle_new_position - half_paddle_height;
            }
            if( paddle_new_position == half_paddle_height || paddle_new_position == height - half_paddle_height ){
                velocity = 0;
            }
            paddle.setVelocity(velocity);

            return this;
        };
        this.setBallVelocity = function(){
            let normal = this.defineCollideNormal();
            if( normal ) {
                this.ball.reflect(normal);
            }
            return this;
        };
        this.restart = function(){
            this.ball.setPosition([width/2, height/2]).setVelocity(rand());
        };
        this.getFieldSize = function(){
            return [height, width];
        };
        this.start = function(){
            this.ball = new Ball(ball_radius);
            this.paddle_left = new Paddle(paddle_width, paddle_height);
            this.paddle_right = new Paddle(paddle_width, paddle_height);
            this.ball.setPosition([width/2, height/2]).setVelocity(rand());
            this.paddle_left.setPosition([0, height/2]);
            this.paddle_right.setPosition([width - paddle_width, height/2]);
        };
        this.tick = function(){
            this.setBallVelocity();
            this.setPaddleVelocity(this.paddle_left);
            this.setPaddleVelocity(this.paddle_right);
            _.each([this.ball, this.paddle_left, this.paddle_right], element => element.move());
        }
    };

})(window.APP);