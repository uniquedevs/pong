'use strict'

;(function(APP){
    let CLASSES = APP.module('Classes')
    ;
    CLASSES.Paddle = function(width, height){
        // private properties
        let
            // position aligns to the center of a bounding box
            _velocity = 0,
            _position = [0,0],
            _width = width,
            _height = height
        ;
        // getters
        this.getWidth = function(){
            return _width;
        };
        this.getHeight = function(){
            return _height;
        };
        this.getVelocity = function(){
            return _velocity;
        };
        this.getPosition = function(){
            return _position;
        };
        // setters
        this.setVelocity = function(velocity){
            _velocity = velocity;
            return this;
        };
        this.setPosition = function(position){
            _position = position;
            return this;
        };
        this.setWidth = function(width){
            _width = width;
            return this;
        };
        this.setHeigth = function(height){
            _height = height;
            return this;
        };
        this.move = function(){
            // move only on vertical direction
            _position[1] += _velocity;
            return this;
        };
    };
})(window.APP);