;(function(APP){
    'use strict';
    let CLASSES = APP.module('Classes');

    CLASSES.Gui = function(){
        let canvas = document.getElementById('canvas'),
            ctx = canvas.getContext("2d"),
            drawBall = function(ball){

                let x = ball.getPosition()[0],
                    y = ball.getPosition()[1],
                    radius = ball.getRadius(),
                    start_angle = 0,
                    end_angle = Math.PI*2
                ;
                ctx.beginPath();
                ctx.strokeStyle = "white";
                ctx.fillStyle = "black";
                ctx.lineWidth=3;
                ctx.arc(x, y, radius, start_angle, end_angle);
                ctx.stroke();
                ctx.fill();
            },
            drawField = function(width, height){
                ctx.clearRect(0,0,width,height);
                ctx.beginPath();
                ctx.rect(0,0,width,height);
                ctx.fillStyle = "black";
                ctx.fill();
                ctx.beginPath();
                ctx.strokeStyle = "white";
                ctx.lineWidth=2;
                ctx.moveTo(width/2,0);
                ctx.lineTo(width/2, height);
                ctx.stroke();
            },
            drawPaddle = function(paddle){
                let [pos_x, pos_y] = paddle.getPosition(),
                    line_width = 2,
                    width = paddle.getWidth(),
                    height = paddle.getHeight()
                    //horizontal_offset = pos_x == 0 ? line_width : line_width
                ;
                ctx.beginPath();
                ctx.strokeStyle = "white";
                ctx.lineWidth = line_width;
                ctx.moveTo(pos_x, pos_y - height/2);
                ctx.lineTo(pos_x + width, pos_y - height/2);
                ctx.lineTo(pos_x + width, pos_y + height/2);
                ctx.lineTo(pos_x, pos_y + height/2);
                ctx.lineTo(pos_x, pos_y - height/2);
                ctx.stroke();
            },
            draw = function( game ){
                let width = game.getFieldSize()[1],
                    height = game.getFieldSize()[0]
                ;
                drawField(width, height);
                drawBall(game.ball);
                drawPaddle(game.paddle_left);
                drawPaddle(game.paddle_right);
            }
        ;
        this.onKeyPress = function(char, game){
            switch(char){
                case 'q':
                    game.paddle_left.setVelocity(-10);
                    break;
                case 'a':
                    game.paddle_left.setVelocity(10);
                    break;
                case 'p':
                    game.paddle_right.setVelocity(-10);
                    break;
                case 'l':
                    game.paddle_right.setVelocity(10);
                    break;
            }
        };
        this.onKeyUp = function(char, game){
            let paddle = char == 'q' || char == 'a' ? 'left' : 'right';
            game['paddle_'+paddle].setVelocity(0);
        };
        this.start = function( game ){
            let width = game.getFieldSize()[1],
                height = game.getFieldSize()[0];
            canvas.width  = width;
            canvas.height = height;
            game.start();

            document.onkeypress = (e) => {

                let char = String.fromCharCode(e.which).toLowerCase();
                this.onKeyPress(char, game);
                return false;
            };

            document.onkeyup = (e) => {

                let char = String.fromCharCode(e.which).toLowerCase();
                this.onKeyUp(char, game);
                return false;
            };

            requestAnimationFrame(function repeater() {
                game.tick();
                draw(game);
                requestAnimationFrame(repeater);
            });
        }
    }
})(window.APP);