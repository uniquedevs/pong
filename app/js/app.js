'use strict'

;(function(APP){
    const   FIELD_WIDTH = 600,
            FIELD_HEIGHT = 400,
            BALL_RADIUS = 20,
            PADDLE_WIDTH = 8,
            PADDLE_HEIGHT = 80;

    document.addEventListener("DOMContentLoaded", function(event) {
        let CLASSES = APP.module('Classes'),
            Game = CLASSES.Game,
            Gui = CLASSES.Gui,
            game = new Game(FIELD_WIDTH,FIELD_HEIGHT,BALL_RADIUS,PADDLE_WIDTH,PADDLE_HEIGHT),
            gui = new Gui()
        ;

        gui.start(game);

    });

    //game.start();
    //game.tick();

    // requestAnimationFrame(function repeater() {
    //     game.tick();
    //     requestAnimationFrame(repeater);
    // });

})(window.APP);